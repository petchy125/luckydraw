import React, { Component } from 'react';
import sweetAlert from 'sweetalert';
import 'sweetalert/dist/sweetalert.css';
import 'highlight.js/styles/googlecode.css';
import './App.styl';
import '../lib/LuckyDraw.css';
import './btn.css';


import Slot from './examples/SlotMachine'
// import Highlight from 'react-highlight';

import firebase from './firebase.js';
import background from "./assets/images/luckydraw/background_star.jpg"

import img from './assets/images/luckydraw/winner.jpg'

let ref


class App extends Component {
  constructor(porps) {
    super(porps);
    this.state = {
      currentTab: 0,
      source2: [],
      prizeIndex: 1,
      ready: false,
      target: 1,
      result: [],
      spin:false,
      resultlist: [],
      spinned: false
    };

    ref= this;
  }

  componentDidMount(){
    document.body.style.background = `url(${background})`;
    document.body.style.backgroundSize = 'cover';
    
    firebase.database().ref('/inventory/').once('value').then(function(snapshot) {
      //var username = (snapshot.val() && snapshot.val().username) || 'Anonymous';
      // ...
      console.log(snapshot.val())
      ref.setState({source: snapshot.val()})

      firebase.database().ref('/raffle/').once('value').then(function(snapshot2) {
        //var username = (snapshot.val() && snapshot.val().username) || 'Anonymous';
        // ...
        let temp = snapshot2.val()
        let arr = []
        let arr2 = []
        arr.push('? ? ?')
        for(let i in temp){
          arr.push(temp[i].name+" - "+temp[i].department.replace("PLT | ", ""))
          arr2.push(i)
        }
        ref.setState({sourceAll: snapshot2.val()})
        firebase.database().ref('/result/').once('value').then(function(snapshot3) {

          console.log(arr)
          let temp3 = snapshot3.val();
          let result = []
          for(let i in temp3){
             for(let t in temp3[i]){
              result.push(temp3[i][t].name)
             }
          }
          //  let arr_filter = arr.filter( value => {
          //   if(result.indexOf(value) == -1){
          //     return value
          //   }
          //  })
           ref.setState({result: result})
          //ref.setState({source2: arr_filter})
          ref.setState({source2: arr})
              if(temp3){
                ref.setState({resultlist: temp3})
              }
                
                  ref.setState({ ready : true})
            
        });
      });
    });

    
  }

  findemID = (data) => {
    let id = -1;
    for(let i in this.state.sourceAll){
    {
      if(this.state.sourceAll[i].name === data){
        id = i
        break;
      }
      
    }
    return id
  }
}

  addResult = (data , index) => {
    //let findID = this.findemID(data);
    let rootRef = firebase.database().ref();
   
      let storesRef = rootRef.child('result/'+this.state.prizeIndex+'/'+index);
      let newStoreRef = storesRef.set({name: data});
    
  

  }


  reload = (e) => {
      
    
        firebase.database().ref('/result/').once('value').then(function(snapshot3) {

          
          let temp3 = snapshot3.val();
          let result = []
          for(let i in temp3){
             for(let t in temp3[i]){
              result.push(temp3[i][t].name)
             }
          }
          
           ref.setState({result: result})
         
          
          ref.setState({resultlist: temp3})
          ref.setState({ ready : true})
        });
     
    
  }

  checkResultDuplicate = (data) => {
      //find index of all result
      let flag = false
      for(let a = 0;a< this.state.result.length;a++){
        let tempObject = this.state.result[a];
        
            if(tempObject === data){
              flag = true
              break;
            }
        
      }
      return flag
  }

  render() {
  
    if(this.state.ready){
      let allgame = []
      
     for(let i=0;i< this.state.source[this.state.prizeIndex].amount;i++){

      allgame.push(<div className="col-md-3" key={"gamediv"+i}> <Slot key={"game"+i} childkey={"game"+i} result={this.state.result} target={this.state.target[i]} list={this.state.source2} index={i} />  </div>)
     }

     let resultList = []
     if(this.state.resultlist){
        if(this.state.resultlist[this.state.prizeIndex]){
          resultList = this.state.resultlist[this.state.prizeIndex].map( value =>{
            return <li> {value.name}</li>
          })
      }
     }
    
     
    
     

      return (
        <div className="container whitebackground">
        
        { /*<Link to="/Table">จับฉลากกลุ่ม</Link> */}
   
        <div className="row" style={{ marginTop : 20}}>
       
          <div className="col-md-6">
          { this.state.prizeIndex > 1 &&
          <button className="btn btn-pru btn-block submit btn-primary" disabled={this.state.spin} onClick={ e => {
            
           
              let count = this.state.prizeIndex -1
            this.setState({ prizeIndex: count, target: 1, spinned: false})
           
          }
            
          }> Back</button>
          }
          </div>
          
            <div className="col-md-6"> 
            { this.state.prizeIndex < this.state.source.length-1 &&
            <button className="btn btn-pru btn-block submit btn-primary" disabled={this.state.spin} onClick={ e => {
              
              
             
                let count = this.state.prizeIndex +1;
              this.setState({ prizeIndex: count, target: 1, spinned: false});
              
            }
            }> Next</button>
          }
          </div>
        </div>
        <div className="row" style={{ marginTop : 20}}> 
            <div className="col-md-6 text-center"> 
                <img src={ img} />
              </div>
            <div className="col-md-6 text-center" style={{ marginTop : 20,color: 'red'}}> 
              <h2 style={{ color: 'red'}}> ของรางวัล : { this.state.source && this.state.source[this.state.prizeIndex].item} </h2>
              <h2 style={{ color: 'red'}}> จำนวน : {this.state.source && this.state.source[this.state.prizeIndex].amount} รางวัล </h2>
              { resultList.length > 0 &&
                <ol>{resultList}</ol> 
              }
              </div>
        </div>
          {/* <Link style={{ padding: '20px' , background: 'blue', color: 'white'}}to="/luckydraw">Spin</Link> */}

          <div className="row" style={{ marginTop : 20}}>
              <div className="col-md-12 text-center">
                <button className="btn btn-success" disabled={ (this.state.resultlist[this.state.prizeIndex] || this.state.spinned) ? true : false } onClick={ e => {
                  if(this.state.source2.length < 3){
                    sweetAlert("Cannot Spin, People is less than 2 ", "success");
                  }
                  else{
                  ref.setState({ spin: true , spinned: true});
                  
                  firebase.database().ref('/result/').once('value').then(function(snapshot3) {
                   
                    let temp3 = snapshot3.val();
                    let result = []
                    for(let i in temp3){
                       for(let t in temp3[i]){
                        result.push(temp3[i][t].name)
                       }
                    }

                     ref.setState({result: result}, () => { 
                     //after set result
                      let items = [];
                      let newItem = []
                      for(let i=0;i< ref.state.source[ref.state.prizeIndex].amount;i++){
                        let value = -1;
                        value = Math.floor(Math.random() * (ref.state.source2.length-2))+1
                        
                      while(items.indexOf(value) != -1 || ref.checkResultDuplicate(ref.state.source2[value])){
                          value = Math.floor(Math.random() * (ref.state.source2.length-2))+1
                      }
                      items.push(value)
                      ref.addResult(ref.state.source2[value] , i)
                      // let index = i
                      // var obj = 
                      //   {index : {}}
                      
                      
                      
                      // newItem.push(myData)
                      }
                      
                      ref.setState({target: items},() =>{
                        firebase.database().ref('/result/').once('value').then(function(snapshot4) {

                          
                          let temp3 = snapshot4.val();
                          
                        setTimeout(function () {
                          ref.setState({ spin: false})
                          ref.setState({ resultlist:temp3})
                        }
                        , 5000);
                        });
                      });
                  })
                  
                    
                  });
                 
                }
                }} > Spin All </button>
                </div>
          </div>
          
        <div className="row">
        {allgame}
          </div>
  
        </div>
      );
    }
    else{
      return(<div> <h2> Loading </h2> </div>)
      
    }
    
  }


}

export default App;
