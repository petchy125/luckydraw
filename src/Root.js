import React from 'react'
import { BrowserRouter,Route ,Switch} from 'react-router-dom'
import home from './home'
import luckydraw from './App'
import Table from './Table'
import Result from './Result'
import Design from './Design'
import Test from './test'


const Root = ({ store  })  =>(
 
   <BrowserRouter>
   <Switch>
        <Route exact path='/' component={home} />
        <Route path='/luckydraw' component={luckydraw} />
        <Route path='/table' component={Table} />
        <Route path='/result' component={Result} />
        <Route path='/design' component={Design} />
        <Route path='/test' component={Test} />
        
      </Switch>

    </BrowserRouter>

)

export default Root