import React from 'react';
import Slot from '../Slot';
//import Random from 'random-js'
import sweetAlert from 'sweetalert';
import firebase from '../firebase.js';


let ref
let resultlist 
let resultTemp = []

class SlotMacine extends React.Component {
  constructor() {
    super();
    this.state = {
      times: 1,
      duration: 3000,
      turn: false,
      target: 1,
      spin: false
    };
    ref = this;
  }

  

  componentWillReceiveProps(nextProps) {
    this.setState({
      target: nextProps.target,
      spin: nextProps.spin,
      resultname: nextProps.resultname ? nextProps.resultname[nextProps.index].name : undefined
    });
  }

  checkResultDuplicate = (data , result) => {
    //find index of all result
    let flag = false
    for(let a = 0;a< result.length;a++){
      let tempObject = result[a];
      
          if(tempObject === data){
            flag = true
            break;
          }
      
    }
    return flag
}

addResult = (data , index) => {
  //let findID = this.findemID(data);
  let rootRef = firebase.database().ref();
 
    let storesRef = rootRef.child('result/'+this.props.prizeIndex+'/'+this.props.index);
    storesRef.set({name: data});
  
    let storesRef2 = rootRef.child('resultlist');
    //let result = resultlist ? resultlist : []
    let result = resultlist
    if(result){
      result.push(data);
      storesRef2.set(result);

    }
    else{
      resultTemp.push(data);
        storesRef2.set(resultTemp);
    }


}

shuffle = (a) => {
  for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
  }
  return a;
}

findIndexFromName = (data , list) => {
  let temp =list.indexOf(data);
  return temp;
}

  
  render() {
    if(this.state.resultname == undefined){
      this.state.resultname = this.props.resultname ? this.props.resultname[this.props.index].name : undefined;
    }
    
    return (
      <div className="row">
        <div className="col-md-12 text-center">
            <Slot
            className="slot"
            duration={5000}
            target={ this.state.target}
            resultname={this.state.resultname ? this.state.resultname : null}
            times={20}
            spin={this.state.spin}
            key={"slotnum"+this.props.childkey}
          >
            { 
              this.props.list.map( name  => (
              <div className="slot-item" key={name}>
                
                  <div className="slot-item-data">
                        <div className="slot-item-name"> {name}</div>
               
                  </div>
              </div>
            ))
          }

          </Slot>
            <button className="btn btn-danger btn-manual-spin" disabled={!this.props.spinned}
            onClick={() =>{
              //this.setState({ spin: true})
               //let value = Math.floor(Math.random() * (this.props.list.length - 2)) + 1;
              //this.setState({ target: value });

              // sweetAlert({
              //   title: "Are you sure?",
              //   text: "Once Spin, you will not be able to recover this imaginary file!",
              //   icon: "warning",
              //   buttons: true,
              //   dangerMode: true,
              // })
              // .then((willDelete) => {
              //   if (willDelete) {
              //     sweetAlert("Poof! Your imaginary file has been deleted!", {
              //       icon: "success",
              //     });
              //   } else {
              //     //sweetAlert("Cancel Spin");
              //   }
              // });
              let rthis = this
              rthis.setState({ spin: true})
              firebase.database().ref('/resultlist/').once('value').then(function(snapshot3) {
                
                let result = snapshot3.val();
              //let result = null
                resultlist = result
              //   let temp3 = result
              //   //let temp3 = this.props.result
              //  // let result = []
              //   for(let i in temp3){
              //      for(let t in temp3[i]){
              //       result.push(temp3[i][t].name)
              //      }
              //   }

                let randomArray = []
                for(let i = 1;i<ref.props.list.length ;i++){
                  randomArray.push(i);
                }
                if(result){
                  randomArray = randomArray.filter( value => {
                    if(ref.findIndexFromName(ref.props.list[value] , result) === -1){
                      return value
                    }
                  })
                }

                  //   let value = -1;
                  //   value = Math.floor(Math.random() * (this.props.list.length-2))+1
                    
                  // while(ref.checkResultDuplicate(this.props.list[value] , result)){
                  //     value = Math.floor(Math.random() * (this.props.list.length-2))+1
                  // }
                //  items.push(value)
                  //let test = this.props.list[value]
                  // let r = new Random();
                  // let value = r.integer(0, ref.props.list.length-1)
                 
                  //randomArray.splice(value, 1);

                
                  if(randomArray.length < ref.props.source[ref.props.prizeIndex].amount ){
                    sweetAlert("Cannot Spin, People is less than Spin length ", "success");
                  }
                  else{
                  randomArray = ref.shuffle(randomArray)
                  let value = randomArray[0]
                  rthis.addResult( ref.props.list[value], ref.props.index)
                  
                  
                  rthis.setState({target: value});
                 
                 
                  setTimeout(function () {
                    rthis.setState({resultname: ref.props.list[value]})
                    rthis.state.resultname = ref.props.list[value]
                    rthis.setState({ spin: false})
                   
                  }
                  , 5000);
                  }
                
                });

            }
            
              
            }
          >
            Spin
          </button>
          </div>
       
     
      </div>
    );
  }
}

export default SlotMacine;