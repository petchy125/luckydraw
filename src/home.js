import React, { Component } from 'react';

import QrReader from 'react-qr-reader'

import firebase from './firebase.js';

//import background from "./assets/images/background.jpg"

import "../node_modules/bootstrap/dist/css/bootstrap.css";

let ref

class App extends Component {

  constructor(props){
    super(props)
    this.state = {
      delay: 200,
      result: 'No result',
      source: undefined,
      total: 0,
      pltl: false,
      id: 0,
      manual: false
    }
    ref = this
    this.handleScan = this.handleScan.bind(this)
  }

  componentDidMount(){
/*    document.body.style.background = `url(${background})`;
    document.body.style.backgroundSize = 'cover';*/
    firebase.database().ref('/HR/').once('value').then(function(snapshot) {
      //var username = (snapshot.val() && snapshot.val().username) || 'Anonymous';
      // ...
      console.log(snapshot.val())
      ref.setState({source: snapshot.val()})
      
    });

    firebase.database().ref('/register/').once('value').then(function(snapshot) {
      //var username = (snapshot.val() && snapshot.val().username) || 'Anonymous';
      // ...
      let count = 0;
      console.log(snapshot.val())
      let items = snapshot.val()
      let temp = []
      for(let item in items)
      {
        temp.push(item)
        count++;
      }
      ref.setState({total: count , register: items})
      firebase.database().ref('/raffle/').once('value').then(function(snapshot7) {
        let temp2 = []
        let items = snapshot7.val()
        for(let item in items)
      {
        temp2.push(item)
        count++;
      }
     let diff = ref.arr_diff(temp,temp2)
      console.log(diff)

      });
    });
  }

   arr_diff = (a1, a2) => {

    var a = [], diff = [];

    for (var i = 0; i < a1.length; i++) {
        a[a1[i]] = true;
    }

    for (var i = 0; i < a2.length; i++) {
        if (a[a2[i]]) {
            delete a[a2[i]];
        } else {
            a[a2[i]] = true;
        }
    }

    for (var k in a) {
        diff.push(k);
    }

    return diff;
}

  findDuplicate = (emID) =>{
    let flag = false
    for(let i in this.state.register){
      if(Number(i) === emID){
        flag = true;
        break;
      }
    }
      return flag;
  } 

  findIDfromEmail = (data) =>{
    let pos = -1;
      for(let i = 0;i< this.state.source.length;i++){
        if(this.state.source[i]["Work Email"].toLowerCase() === data.toLowerCase()){
          pos = i
         
          break;
        }
      }
      return pos
  }

  findIDfromID = (data) =>{
    let pos = -1;
      for(let i = 0;i< this.state.source.length;i++){
        if(this.state.source[i]["Employee ID"] === Number(data)){
          pos = i
         
          break;
        }
      }
      return pos
  }

  handleScan(data , typeScan){
    try{

      firebase.database().ref('/register/').once('value').then(function(snapshot) {
        let items = snapshot.val()
        
        ref.setState({register: items})
          if(data){
          // let CheckEmail = 
          
          
            if(ref.validateEmail(data)){
              //PTL
              
              let emIDpos = ref.findIDfromEmail(data)
              //if found user
              if(emIDpos !== -1){
                let rootRef = firebase.database().ref();
              
                let emID = ref.state.source[emIDpos]["Employee ID"]
                let name = ref.state.source[emIDpos]["Name Eng"]
                let department = ref.state.source[emIDpos]["Department"]
                let email = ref.state.source[emIDpos]["Work Email"]
  
                if(ref.findDuplicate(emID)){
                  //if duplicate
                  ref.setState({
                    result: "You are already register!!!, "+emID+" : "+name,
                    flag: "error"
                  })
                }
                else{

                      let storesRef = rootRef.child('register/'+emID);
                      let newStoreRef = storesRef.set({name: name , employeeID: emID , department: department ,type: typeScan});
      
                      if(ref.state.source[emIDpos]["Department Payroll"].toLowerCase() !== "SMT Member".toLowerCase()){
                        //non-smt can raffle
                        
                        let storesRef = rootRef.child('raffle/'+emID);
                        let newStoreRef = storesRef.set({name: name , employeeID: emID , department: department ,type: typeScan});
              
                      }
                      
      
                      ref.setState({
                        result: emID+" : "+name+" register success",
                        flag: ""
                      })
              
                      firebase.database().ref('/register/').once('value').then(function(snapshot) {
                        //var username = (snapshot.val() && snapshot.val().username) || 'Anonymous';
                        // ...
                        let count = 0;
                        console.log(snapshot.val())
                        let items = snapshot.val()
                        for(let item in items)
                        {
                          count++;
                        }
                        ref.setState({total: count})
                        
                      });
                    }
              
                
              }
              else{
                ref.setState({
                  result: "User not found",
                  flag: "error"
                })
              }
              
              
            }
            else if(!isNaN(data)){
              //TSR
              let emIDpos = ref.findIDfromID(data)
              if(emIDpos !== -1){
                let rootRef = firebase.database().ref();

                let emID = ref.state.source[emIDpos]["Employee ID"]
                let name = ref.state.source[emIDpos]["Name Eng"]
                let department = ref.state.source[emIDpos]["Department"]
  
                if(ref.findDuplicate(emID)){
                    ref.setState({
                      result: "You are already register!!!, "+emID+" : "+name,
                      flag: "error"
                    })
                }
                else{
                  let storesRef = rootRef.child('register/'+emID);
                  let newStoreRef = storesRef.set({valid:true});
  
                  if(ref.state.source[emIDpos]["Department Payroll"].toLowerCase() !== "SMT Member".toLowerCase()){
                    //non-smt can raffle  
                    let storesRef = rootRef.child('raffle/'+emID);
                    let newStoreRef = storesRef.set({name: name , employeeID: emID , department: department , type: typeScan});
                  }
                
                    ref.setState({
                      result: emID+" : "+name+" register success",
                      flag: ""
                    })
                  
                
                  firebase.database().ref('/register/').once('value').then(function(snapshot) {
                    //var username = (snapshot.val() && snapshot.val().username) || 'Anonymous';
                    // ...
                      let count = 0;
                      console.log(snapshot.val())
                      let items = snapshot.val()
                      for(let item in items)
                      {
                        count++;
                      }
                      ref.setState({total: count})
                    
                  });
                }

              }

              else{
                ref.setState({
                  result: "User not found",
                  flag: "error"
                })
              }
            
            
            }
            else{
              ref.setState({
                result: "data is not valid",
                flag: "error"
              })
            }
            
          }
          
          
        });
        }
        catch(error){
          this.setState({
            result: "User not found",
            flag: "error"
          })
        }
  }

   validateEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

  handleError(err){
    console.error(err)
  }

  render(){
    if(this.state.source){
      return(
        <div>
        
        <center>
        <h2 className="welcome-title"> Welcome Staff Party</h2>
          <QrReader
            delay={this.state.delay}
            onError={this.handleError}
            onScan={(e) => this.handleScan(e, "scan")}
            style={{ width: '25%' }}
            />
          <h2 style={{ margin: 30 , color: this.state.flag == 'error' ? "red" : "green"}}>{this.state.result}</h2>

          <p style={{ margin: 30}}> Total register : {this.state.total} / 829</p>

          <p style={{ margin: 30}}> <button className="btn btn-pru submit btn-primary" disabled={this.state.submiting} onClick={ e => this.setState({ manual : !this.state.manual})} > Cannot scan ? </button> </p>
          
       <div className="container" style={{ display: this.state.manual ? 'block': 'none' , marginBottom : '30px'}}>
          <h2> Manual register</h2>
          <form className="form-signin" onSubmit={this.onSubmit}>
          <div className="row">
          <div className="col-md-12">
          <input type="text" className="form-control" style={{ marginBottom : '20px'}} id="id" onChange={ (e) => this.setState({id: e.target.value}) } placeholder="Email (without @prudential) or Employee ID" /> 
          </div>

          </div>

          <input className="btn btn-pru btn-block submit btn-primary" type="submit" value="Submit" disabled={this.state.submiting} />
          </form>
        </div>
        </center>
        </div>
      )
    }
    else{
      return( <div>
          <h1> Loading</h1>
        </div>)
    }
   
  }

  onSubmit(e) {
    e.preventDefault();
    if(!isNaN(ref.state.id))
    {
      //if Employee ID
      ref.handleScan(ref.state.id , "manual")
    }
    else{
      ref.handleScan(ref.state.id+"@prudential.co.th" , "manual")
    }
      
    
  }
}

export default App;
