import React, { Component } from 'react';


import firebase from './firebase.js';

//import background from "./assets/images/luckydraw/gold_background.jpg"
import winner from "./assets/images/luckydraw/jackpot.jpg"
//import './result.css'
//require('./result.css')

let ref


class Result extends Component {
  constructor(porps) {
    super(porps);
    this.state = {
      currentTab: 0,
      source2: [],
      prizeIndex: 1,
      ready: false,
      target: 1,
      loading: '',
      result1: [],
      result2: []
    };

    ref= this;
  }

  componentDidMount(){
      //document.body.style.background = 'white';
    //  document.body.style.backgroundSize = 'cover';
    firebase.database().ref('/inventory/').once('value').then(function(inv1) {
      let temp = inv1.val();
      ref.setState({ inventory : temp})
      firebase.database().ref('/inventory-table/').once('value').then(function(inv2) {
        let temp2 = inv2.val();
      
        ref.setState({ inventorytable : temp2})
        firebase.database().ref('/result/').once('value').then(function(snapshot) {
          let temp2 = snapshot.val()
          if(temp2 !== undefined){
            ref.setState({result1:  snapshot.val()})
          }
          else{
            ref.setState({result1:  []})
          }
          
          
          firebase.database().ref('/result-table/').once('value').then(function(snapshot2) {
          
            let temp3 = snapshot2.val();
            let dataTemp = ""
            if(temp3 !== undefined){
               dataTemp = { cells:[temp3] };
              ref.setState({result2: dataTemp })
            }
            else{
               dataTemp = { cells:[] };
              ref.setState({result2: dataTemp })
            }
            ref.setState({ ready : true})
          });
        });
      });
  });
    

    
  }



  reload = (e) => {
      this.setState({ loading: 'loading'})

    firebase.database().ref('/inventory/').once('value').then(function(inv1) {
      let temp = inv1.val();
      
      ref.setState({ inventory : temp})
      firebase.database().ref('/inventory-table/').once('value').then(function(inv2) {
        let temp2 = inv2.val();
      
        ref.setState({ inventorytable : temp2})
        firebase.database().ref('/result/').once('value').then(function(snapshot) {
        
          let temp2 = snapshot.val()
          if(temp2){
              ref.setState({result1:  snapshot.val()})
          }
          else{
            ref.setState({result1:  []})
          }
          
          firebase.database().ref('/result-table/').once('value').then(function(snapshot2) {
            let temp3 = snapshot2.val();
            let dataTemp = "";
            if(temp3 !== undefined){
               dataTemp = { cells:[temp3] };
              ref.setState({result2: dataTemp })
            }
            else{
               dataTemp = { cells:[] };
              ref.setState({result2: dataTemp })
            }
            
            ref.setState({ loading: 'load finish'})
            
          });
        });
      });
  });
    
  }

  

  render() {
    // document.body.style.background = `url(${background})`;
    // document.body.style.backgroundSize = 'cover';
    if(this.state.ready){
      
      // const result1 = this.state.result1 ? [] : this.state.result1.map( (item,index) => {
      //  let temp = item.map( value =>{
      //      return <li key={'inner'+value}> {value.name}</li>
      //   })
      //let temp = []
        // for(let it in item){
        //   temp.push( <li key={'inner'+it}> {item[it].name}</li>)
        // }
        const result1Root = this.state.result1
        // let temp = []
         let result1 = []
         if(result1Root){
         
            result1 = Object.keys(result1Root).map( (value,index) => {

              let temp = Object.keys(result1Root[Number(value)]).map( (value1,index1) => {
                  return <li key={'inner'+value1}> {result1Root[Number(value)][value1].name}</li>
              })
              return <li key={'outer'+index}><h4> {ref.state.inventory[Number(value)].item+" - จำนวน "+ref.state.inventory[Number(value)].amount +" ชิ้น"}</h4>
              <ul className="list-name"> {temp} </ul>
               </li>
            })
            // let index = Number(it)
            // let innerArray = result1Root[index];
            // result1 =  innerArray.map( value => {
             // return <li key={'outer'+index}><h4> {ref.state.inventory[index+1].item+" - จำนวน "+ref.state.inventory[index+1].amount +" ชิ้น"}</h4>
             //   <ul> {temp} </ul></li>
           //}
         }
       
         console.log(result1)
        //console.log(result1)
      //   return (<li key={'outer'+index}><h4> {ref.state.inventory[index+1].item+" - จำนวน "+ref.state.inventory[index+1].amount +" ชิ้น"}</h4>
      //   <ul> {temp} </ul></li>)
      // })

      const result2 = this.state.result2.cells.length === 0 ? [] : this.state.result2.cells.map( item => {
        let temp = item.map( (value,index) =>{
        
           let insideValue = []
            for(let it in value){
              insideValue.push( <li key={'item'+value[Number(it)].table}> โต๊ะเลขที่ : {value[Number(it)].table}</li>)
            }

          return  <li><h4> {ref.state.inventorytable[index].item+" - จำนวน "+ref.state.inventorytable[index].amount +" ชิ้น"}</h4>
          <ul className="list-name"> {insideValue} </ul></li>
         })
         return temp
       })
        
      
      return (
        <div className="container firsttab result" style={{ color: 'white'}}>
          <div className="row">
          <div className="col-md-12" style={{ margin: 30}}>
          <button className="btn btn-primary" onClick={ (e) => this.reload()} > Reload </button>  { this.state.loading}
          </div>
            <div className="col-md-12 text-center" style={{ marginBottom: 30}}>
            
            <img src={winner} width="400" />
            </div>
              <div className="col-md-6">
                
                    <h2 className=""> Lucky Draw Result</h2>
                
                  <div className="result">
                    <div className="row" style={{ marginLeft: 20}}>
                    { this.state.result1 ?
                      <ol>
                      {result1}
                      </ol>
                      :
                      'ไม่มีข้อมูล'
                    }
                    </div>
                  </div>
                    
              </div>
              <div className="col-md-6">
                 <div className="col-md-12">
                    <h2> Table - Lucky Draw Result</h2>
                  </div>
                  <div className="col-md-12">
                    <div className="row" style={{ marginLeft: 20}}>
                    { this.state.result2.cells.length > 0 ?
                    <ol>
                    {result2}
                    </ol>

                    :
                    'ไม่มีข้อมูล'
                    }
                    </div>
                  </div>
              </div>
          </div>
        </div>

      )
    }
    else{
      return(<div> <h2> Loading </h2> </div>)
      
    }
    
  }


}

export default Result;
