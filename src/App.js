import React, { Component } from 'react';


import firebase from './firebase.js';

import background from "./assets/images/luckydraw/gold_background.jpg"
import winner from "./assets/images/luckydraw/jackpot.jpg"
//import './result.css'
//require('./result.css')

import sweetAlert from 'sweetalert';
import 'sweetalert/dist/sweetalert.css';
import 'highlight.js/styles/googlecode.css';
import './App.styl';
import '../lib/LuckyDraw.css';
import './btn.css';


import Slot from './examples/SlotMachine'
import Chance from 'chance'

// import Highlight from 'react-highlight';


import img from './assets/images/luckydraw/winner.jpg'



let ref
let resultTemp = []

class App extends Component {
  constructor(porps) {
    super(porps);
    this.state = {
      currentTab: 0,
      source2: [],
      prizeIndex: 1,
      ready: false,
      target: 1,
      result: [],
      spin:false,
      resultlist: undefined,
      spinned: false
    };

    ref= this;
  }

 componentDidMount(){
/*    document.body.style.background = `url(${background})`;
    document.body.style.backgroundSize = 'cover';*/

    document.title = "Lucky Draw";
    
    firebase.database().ref('/inventory/').once('value').then(function(snapshot) {
      //var username = (snapshot.val() && snapshot.val().username) || 'Anonymous';
      // ...
      console.log(snapshot.val())
      ref.setState({source: snapshot.val()})

      firebase.database().ref('/raffle/').once('value').then(function(snapshot2) {
        //var username = (snapshot.val() && snapshot.val().username) || 'Anonymous';
        // ...
        let temp = snapshot2.val()
        let arr = []
        let arr2 = []
        arr.push('? ? ?')
        for(let i in temp){
          arr.push(temp[i].name+" - "+temp[i].department.replace("PLT | ", ""))
          arr2.push(i)
        }
        ref.setState({sourceAll: snapshot2.val()})
        firebase.database().ref('/result/').once('value').then(function(snapshot3) {

          console.log(arr)
          let temp3 = snapshot3.val();
          let result = []
          let targetIndex = []
          for(let i in temp3){
             for(let t in temp3[i]){
              result.push(temp3[i][t].name);
             // targetIndex.push()
             if(arr){
              targetIndex.push((ref.findIndexFromName(temp3[i][t].name , arr)));
             }
             
             }
          }
          //  let arr_filter = arr.filter( value => {
          //   if(result.indexOf(value) == -1){
          //     return value
          //   }
          //  })
           ref.setState({result: result})

          // this.state.target[i]
          // resultList = this.state.resultlist[this.state.prizeIndex].map( value =>{
          //   return <div className="obj-wood-table-luckydraw"> {value.name}</div>
          // })
          //ref.setState({source2: arr_filter})
          ref.setState({source2: arr})
              if(temp3){
             console.log(temp3)
                ref.setState({resultlist: temp3 })
               // ref.setState({ spin : true})
                console.log('targetIndex')
                console.log(targetIndex)
                ref.setState({target: targetIndex})

                if(temp3[ref.state.prizeIndex]){
                  ref.setState({  spinned: true})
                 
                }
              }
              else{
                ref.setState({  spinned: false})
              }
                ref.setState({ spin : false})


               
                
              
                  ref.setState({ ready : true})
                  ref.actionButton('reload');
            
        });
      });
    });

    
  }

  findIndexFromName = (data , list) => {
    let temp =list.indexOf(data);
    return temp;
  }

  shuffle = (a) => {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

  spinAction = () => {
    
      ref.setState({ spin: true , spinned: true});

    
      //Random.shuffle(randomArray)
                    
      firebase.database().ref('/resultlist/').once('value').then(function(snapshot3) {
       
        let result = snapshot3.val();
        // let result = []
        // for(let i in temp3){
        //    for(let t in temp3[i]){
        //     result.push(temp3[i][t].name)
        //    }
        // }
        //creata all list to random
        let randomArray = []
      for(let i = 1;i<ref.state.source2.length ;i++){
        randomArray.push(i);
      }
      if(result){
        randomArray = randomArray.filter( value => {
          if(ref.findIndexFromName(ref.state.source2[value] , result) === -1){
            return value
          }
        })
      }
      if(randomArray.length < ref.state.source[ref.state.prizeIndex].amount ){
        sweetAlert("Cannot Spin, People is less than Spin length ", "success");
      }
      else{
      randomArray = ref.shuffle(randomArray)
      
        
        console.log('randomArray')
        console.log(randomArray)
  
         ref.setState({result: result}, () => { 
         //after set result
          let items = [];
          let newItem = []
         
          
          for(let i=0;i< ref.state.source[ref.state.prizeIndex].amount;i++){
           
        
          //   let value = -1;
          //   value = Math.floor(Math.random() * (ref.state.source2.length-2))+1
          // while(items.indexOf(value) !== -1 || ref.checkResultDuplicate(ref.state.source2[value])){
          //     value = Math.floor(Math.random() * (ref.state.source2.length-2))+1
          // }
           //items.push(value)
            let value = randomArray[0];
             items.push(value)
            ref.addResult(ref.state.source2[value] , i)
            randomArray.splice(0,1)
           
          }
          console.log('items')
          console.log(items)
          ref.setState({target: items},() =>{
            firebase.database().ref('/result/').once('value').then(function(snapshot4) {
  
              
              let temp3 = snapshot4.val();
              
            setTimeout(function () {
              ref.setState({ spin: false})
              ref.setState({ resultlist:temp3})
            }
            , 5000);
            });
          });
      })
    }
        
      });
    
  
  }

  findemID = (data) => {
    let id = -1;
    for(let i in this.state.sourceAll){
    {
      if(this.state.sourceAll[i].name === data){
        id = i
        break;
      }
      
    }
    return id
  }
}

  addResult = (data , index) => {
    //let findID = this.findemID(data);
    let rootRef = firebase.database().ref();
   
      let storesRef = rootRef.child('result/'+this.state.prizeIndex+'/'+index);
      storesRef.set({name: data});
    
      let storesRef2 = rootRef.child('resultlist');
      let result = this.state.result
      if(result){
        result.push(data);
        storesRef2.set(result);
  
      }
      else{
        
       
         
        resultTemp.push(data);
          storesRef2.set(resultTemp);
        
      }
     
  }


  reload = (e) => {
      
    
        firebase.database().ref('/result/').once('value').then(function(snapshot3) {

          
          let temp3 = snapshot3.val();
          let result = []
          for(let i in temp3){
             for(let t in temp3[i]){
              result.push(temp3[i][t].name)
             }
          }
          
           ref.setState({result: result})
         
          
          ref.setState({resultlist: temp3})
          ref.setState({ ready : true})
        });
     
    
  }

  checkResultDuplicate = (data) => {
      //find index of all result
      let flag = false
      if(this.state.result){
        for(let a = 0;a< this.state.result.length;a++){
          let tempObject = this.state.result[a];
          
              if(tempObject === data){
                flag = true
                break;
              }
          
        }
      }
      
      return flag
  }

  actionButton = (action) => {
    ref.setState({ spin: true})

    ref.reload();
    let count = 0
    if(action === "next"){
       count = this.state.prizeIndex +1
    }
    else if(action === "back"){
      count = this.state.prizeIndex -1
    }
    else{
      count = this.state.prizeIndex
    }
    
    this.setState({ prizeIndex: count, target: 1})

    if(this.state.resultlist){
      if(this.state.resultlist[count]){
          this.setState({  spinned: true})
      }
      else{
        this.setState({  spinned: false})
      }
    }
    else{
      this.setState({  spinned: false})
    }
    ref.setState({ spin: false})
  }
    
 
  
  

  render() {
    // document.body.style.background = `url(${background})`;
    // document.body.style.backgroundSize = 'cover';
    if(this.state.ready){
      let allgame = []
      
      for(let i=0;i< this.state.source[this.state.prizeIndex].amount;i++){
 
       allgame.push(<div className="col-md-3" key={"gamediv"+i}>
        <Slot key={"game"+i} childkey={"game"+i} source={this.state.source} 
        spin={this.state.spin}
        spinned={this.state.spinned}
        result={this.state.result} target={ref.state.target[i]} list={this.state.source2} 
        prizeIndex={this.state.prizeIndex} resultname={this.state.resultlist ? this.state.resultlist[this.state.prizeIndex] : undefined} index={i} />  </div>)
      }
 
      // let resultList = []
      // if(this.state.resultlist){
      //    if(this.state.resultlist[this.state.prizeIndex]){
      //      resultList = this.state.resultlist[this.state.prizeIndex].map( value =>{
      //        return <div className="obj-wood-table-luckydraw"> {value.name}</div>
      //      })
      //  }
      // }

      return(
      <div className="main-wrap"> 
        <div className="top-nav">
      { this.state.prizeIndex > 1 && <span className="back" onClick={ e => this.actionButton('back') }>Back</span> }
      { this.state.prizeIndex < this.state.source.length-1 &&  <span className="next" onClick={ e => this.actionButton('next') }>next</span> }
        </div>
        <div className="title"></div> 


        <h2 className="title-prize">ของรางวัล : { this.state.source && this.state.source[this.state.prizeIndex].item} จำนวน : {this.state.source && this.state.source[this.state.prizeIndex].amount} รางวัล</h2>

        <div className="content-wrap">
            
            <div className="content-container">

                <div className="row" style={{ marginTop : 20}}>

          </div>
          
        <div className="row slot-wrap">
              {allgame}
          </div>
                  
                  
            </div>

            <span className="btn-spin-all" style={this.state.spinned ? {'border-color': 'transparent transparent transparent gray',
    cursor: 'not-allowed'} : {cursor: 'pointer'} }  onClick={ e => {
                if(this.state.spinned){
                    sweetAlert("Cannot Spin All Button, 2rd times", "success");
                }
                else{
                    this.spinAction();
                }
            }} > Spin All </span>


        </div>




      </div>)
    }
    else{
      return(<div className="main-wrap">  <h2> Loading </h2> </div>)
      
    }
    
  }


}

export default App;
