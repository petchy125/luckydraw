import React, { Component } from 'react';
import sweetAlert from 'sweetalert';
import LuckyDraw from './LuckyDraw';
import 'sweetalert/dist/sweetalert.css';
import 'highlight.js/styles/googlecode.css';
import './App.styl';
import '../lib/LuckyDraw.css';
import './btn.css';

// import Highlight from 'react-highlight';

import firebase from './firebase.js';

import background from "./assets/images/luckydraw/background_star.jpg"

//require('./result.css')

let ref
let arr = []

class Design extends Component {
  constructor(porps) {
    super(porps);
    this.state = {
      currentTab: 0,
      source2: [],
      prizeIndex: 1,
      ready: false,
      target: 1,
      spinFlag: false,
      count: 1,
      resulttable: []
    };

    ref= this;
    arr = []
  }

  componentDidMount(){
    /* document.body.style.background = `url(${background})`;
    document.body.style.backgroundSize = 'cover'; */
    document.title = "Table";
    firebase.database().ref('/inventory-table/').once('value').then(function(snapshot) {
      //var username = (snapshot.val() && snapshot.val().username) || 'Anonymous';
      // ...
      console.log(snapshot.val())
      ref.setState({source: snapshot.val()})

      firebase.database().ref('/raffle-table/').once('value').then(function(snapshot2) {
        //var username = (snapshot.val() && snapshot.val().username) || 'Anonymous';
        // ...
        let temp = snapshot2.val()
        ref.setState({sourceAll: temp})
        
        let arr2 = []
        for(let i in temp){
          arr.push(Number(i))
          arr2.push(i)
        }
        console.log(arr)
        firebase.database().ref('/result-table/').once('value').then(function(snapshot3) {

          let temp3 = snapshot3.val()
          let arr3 = []
          for(let i in temp3){
            for(let j in temp3[i]){
              console.log(j);
              arr3.push(Number(j))
            }
            
            //}
          }
          console.log(arr3)
          //for(let j in temp3[i]){

        let arr_filter = arr.filter( value => {
          if(arr3.indexOf(value) === -1){
            return value
          }
          
        })
        ref.setState({source2: arr_filter})
        if(temp3){
          ref.setState({ resulttable: temp3})
        }
        let test1 = 0;
          for(let g in ref.state.resulttable[ref.state.prizeIndex]){
            test1++;
          }
          let test2 = ref.state.source[ref.state.prizeIndex].amount
          if(test1 == test2){
          ref.setState({ spinFlag : true})
        }
        else{
          ref.setState({ spinFlag : false})
        }
        
        ref.setState({ ready : true})
        });
        
      });
    });
    
  }

  uploadResult = () => {
    firebase.database().ref('/inventory-table/').once('value').then(function(snapshot) {
      //var username = (snapshot.val() && snapshot.val().username) || 'Anonymous';
      // ...
      console.log(snapshot.val())
      ref.setState({source: snapshot.val()})

      firebase.database().ref('/raffle-table/').once('value').then(function(snapshot2) {
        //var username = (snapshot.val() && snapshot.val().username) || 'Anonymous';
        // ...
        let temp = snapshot2.val()
        ref.setState({sourceAll: temp})
        
        let arr2 = []
        arr = []
        for(let i in temp){
          arr.push(Number(i))
          arr2.push(i)
        }
        
        firebase.database().ref('/result-table/').once('value').then(function(snapshot3) {

          let temp3 = snapshot3.val()
          let arr3 = []
          for(let i in temp3){
            for(let j in temp3[i]){
              console.log(j);
              arr3.push(Number(j))
            }
            
            //}
          }
          console.log(arr3)
          //for(let j in temp3[i]){

        let arr_filter = arr.filter( value => {
          if(arr3.indexOf(value) === -1){
            return value
          }
          
        })
        console.log(arr_filter)
       // ref.setState({source2: arr_filter})
        
        if(temp3){
          ref.setState({ resulttable: temp3})
        }
        let test1 = 0;
        for(let g in ref.state.resulttable[ref.state.prizeIndex]){
          test1++;
        }
        let test2 = ref.state.source[ref.state.prizeIndex].amount
        if(test1 === test2){
          ref.setState({ spinFlag : true})
        }
        else{
          ref.setState({ spinFlag : false})
        }
        ref.setState({ ready : true})
        });
        
      });
    });
    
  }

  setSpin = () => {
    let spinFlag = !this.state.spinFlag
    ref.setState({ spinFlag : spinFlag})
  }

  reload = () => {
    
      firebase.database().ref('/inventory-table/').once('value').then(function(snapshot) {
        //var username = (snapshot.val() && snapshot.val().username) || 'Anonymous';
        // ...
        console.log(snapshot.val())
        ref.setState({source: snapshot.val()})
  
        firebase.database().ref('/raffle-table/').once('value').then(function(snapshot2) {
          //var username = (snapshot.val() && snapshot.val().username) || 'Anonymous';
          // ...
          let temp = snapshot2.val()
          ref.setState({sourceAll: temp})
          
          let arr2 = []
          arr = []
          for(let i in temp){
            arr.push(Number(i))
            arr2.push(i)
          }
          
          firebase.database().ref('/result-table/').once('value').then(function(snapshot3) {
  
            let temp3 = snapshot3.val()
            let arr3 = []
            for(let i in temp3){
              for(let j in temp3[i]){
                console.log(j);
                arr3.push(Number(j))
              }
              
              //}
            }
            console.log(arr3)
            //for(let j in temp3[i]){
  
          let arr_filter = arr.filter( value => {
            if(arr3.indexOf(value) === -1){
              return value
            }
            
          })
          console.log(arr_filter)
          ref.setState({source2: arr_filter})
          
          if(temp3){
            ref.setState({ resulttable: temp3})
          }
          let test1 = 0;
          for(let g in ref.state.resulttable[ref.state.prizeIndex]){
            test1++;
          }
          let test2 = ref.state.source[ref.state.prizeIndex].amount
          if(test1 === test2){
            ref.setState({ spinFlag : true})
          }
          else{
            ref.setState({ spinFlag : false})
          }
          ref.setState({ ready : true})
          });
          
        });
      });
      
    
  }

  findemID = (data) => {
    let id = -1;
    for(let i in this.state.sourceAll){
    {
      if(this.state.sourceAll[i].name === data){
        id = i
        break;
      }
      
    }
    return id
  }
}

  addResult = (data) => {
    let findID = this.findemID(data);
    let rootRef = firebase.database().ref();
    let storesRef = rootRef.child('result/'+findID);
    let newStoreRef = storesRef.set({name:data});

  }

  actionButton = (action) => {

    let count = 0
    if(action === "next"){
      count = this.state.prizeIndex +1
    }
    else{
      count = this.state.prizeIndex -1
    }
    if(count < this.state.source.length)
    this.setState({ prizeIndex: count ,spinFlag : false});
   
    this.uploadResult();
  }

  onSuccessDrawReturnAction = (drawNumber) => {
    let rootRef = firebase.database().ref();
    let storesRef = rootRef.child('result-table/'+this.state.prizeIndex+'/'+this.state.source2[drawNumber]);
    let newStoreRef = storesRef.set({table: this.state.source2[drawNumber]});

    setTimeout(()=>{
        sweetAlert("Table : "+this.state.source2[drawNumber], 'Congratulations !', "success");
        let tempCount = this.state.count+1;
        this.uploadResult();
    },2000)




    
  }

 

  

  render() {
    
    let resulttable = []
      if(this.state.resulttable[this.state.prizeIndex]){
          // for(let i in this.state.resulttable[this.state.prizeIndex]){
          //   resulttable.push(<div className="obj-wood-table"> {i} </div>)
          // }
          resulttable = Object.keys(this.state.resulttable[this.state.prizeIndex]).map( value => {
            return <div className="obj-wood-table" key={"table"+value}> {value} </div>
          })
           
        
      }
      
    if(this.state.ready){
      return(
      <div className="main-wrap"> 
        <div className="top-nav">
      { this.state.prizeIndex > 1 && <span className="back" onClick={ e =>  this.actionButton("back") }>Back</span> }
      { this.state.prizeIndex < this.state.source.length-1 &&  <span className="next" onClick={ e =>  this.actionButton("next") }>next</span> }
        </div>
        <div className="title"></div> 


        <h2 className="title-prize">ของรางวัล : { this.state.source && this.state.source[this.state.prizeIndex].item}
จำนวน : {this.state.source && this.state.source[this.state.prizeIndex].amount} โต๊ะ</h2>

        <div className="content-wrap">
            
            <div className="content-container">

                <div className="wood-tables">
                        <div className="text-center wood-tables-title">ผลรางวัล</div>
                     {resulttable}
                </div>
                <center>
                <LuckyDraw
                      width={800}
                      height={800}
                      wheelSize={640}
                      range={this.state.source2.length}
                      innerRadius={200}
                      outerRadius={320}
                      turns={6}
                      rotateSecond={5}
                      showInnerLabels
                      drawLimitSwitch
                      drawLimit={1000}
                      fontColor={'#000'}
                      fontSize={'14px'}
                      writingModel={'tb'}
                      drawButtonLabel={'start'}
                      disabled={this.state.spinFlag}
                      textArray={this.state.source2}
                      parentRef={this}
                      onSuccessDrawReturn={drawNumber => this.onSuccessDrawReturnAction(drawNumber) }
                      onOutLimitAlert={limit => {
                        if (limit) {
                          sweetAlert("Oops...", "out of limits!!", "error");
                        }
                      }}
                      />
                </center>
                  
            </div>


        </div>




      </div>)
    }
    else{
      return(<div> <h2> Loading </h2> </div>)
      
    }
    
  }


}

export default Design;
/*
<LuckyDraw
                      width={600}
                      height={350}
                      wheelSize={1000}
                      range={this.state.source2.length}
                      innerRadius={250}
                      outerRadius={500}
                      turns={6}
                      rotateSecond={5}
                      showInnerLabels
                      drawLimitSwitch
                      drawLimit={1000}
                      fontColor={'#000'}
                      fontSize={'20px'}
                      writingModel={'tb'}
                      drawButtonLabel={'start'}
                      disabled={this.state.spinFlag}
                      textArray={this.state.source2}
                      parentRef={this}
                      onSuccessDrawReturn={drawNumber => this.onSuccessDrawReturnAction(drawNumber) }
                      onOutLimitAlert={limit => {
                        if (limit) {
                          sweetAlert("Oops...", "out of limits!!", "error");
                        }
                      }}
                      />
*/